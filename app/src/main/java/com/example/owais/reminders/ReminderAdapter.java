package com.example.owais.reminders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class ReminderAdapter extends ArrayAdapter<ReminderModel> {

    private LayoutInflater mInflater;
    private Context mContext;
    private String TAG = getClass().getSimpleName();

    public ReminderAdapter(Context context, int resource, List<ReminderModel> objects) {
        super(context, resource, objects);
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            // if the view passed is null, inflate one
            convertView = mInflater.inflate(R.layout.adapter_reminder_layout, null);

            // save views in the viewHolder
            ReminderModel reminder = getItem(position);
            viewHolder = new ViewHolder(reminder.getReminderName(), reminder.getReminderTime(), reminder.getReminderRepeat(), reminder.getReminderStartDate());

            ((TextView) convertView.findViewById(R.id.adapter_reminder_name)).setText(viewHolder.mReminderName);
            ((TextView) convertView.findViewById(R.id.adapter_reminder_time)).setText(viewHolder.mReminderTime);
            ((TextView) convertView.findViewById(R.id.adapter_reminder_date)).setText(viewHolder.mReminderDate);
            ((TextView) convertView.findViewById(R.id.adapter_reminder_repeat)).setText(viewHolder.mReminderRepeat);

            convertView.setTag(viewHolder);
        }
        return convertView;
    }

    public class  ViewHolder {
        public String mReminderName;
        public String mReminderTime;
        public String mReminderRepeat;
        public String mReminderDate;

        public ViewHolder(String mReminderName, String mReminderTime, String mReminderRepeat, String mReminderDate) {
            this.mReminderName = mReminderName;
            this.mReminderTime = mReminderTime;
            this.mReminderRepeat = mReminderRepeat;
            this.mReminderDate = mReminderDate;
        }
    }
}
