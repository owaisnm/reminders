package com.example.owais.reminders;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by owais on 9/25/15.
 */
public class NotificationRealmModel extends RealmObject {

    @PrimaryKey
    private int notificationID;

    private int reminderID;
    private String time;

    public int getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(int notificationID) {
        this.notificationID = notificationID;
    }

    public int getReminderID() {
        return reminderID;
    }

    public void setReminderID(int reminderID) {
        this.reminderID = reminderID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}