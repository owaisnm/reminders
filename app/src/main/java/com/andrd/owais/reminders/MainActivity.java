package com.andrd.owais.reminders;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements DatePickerFragment.DatePickerDialogListener, TimePickerFragment.TimePickerDialogListener {

    private String TAG = getClass().getSimpleName();
    public Toolbar mToolbar;
    private FloatingActionButton floatingActionButton;

    private AlertDialog.Builder reminderDialogBuilder;
    private AlertDialog reminderDialog;

    private LinearLayout reminderDateLayout;
    private LinearLayout reminderTimeLayout;
    private LinearLayout reminderRepetitionLayout;

    private View reminderDialogView;
    private Handler mReminderHandler;

    ReminderModel reminderModel;
    private EditText reminderName;
    private TextView reminderStartDate;
    private TextView reminderTime;
    private TextView reminderCount;
    private int repeatCount=0;
    private String repeatText = "No repetition";
    private ListView reminderListView;
    private ReminderAdapter reminderAdapter;
    private ArrayList<ReminderModel> reminderList;

    private boolean isNewReminder = true;
    private int selectedReminderID;

    private Settings mSettings;
    private Realm mRealm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mReminderHandler = new Handler();
        mSettings = new Settings(this);
        reminderList = new ArrayList<>();
        reminderAdapter = new ReminderAdapter(this, 0, reminderList);

        setViews();
        setReminderListView();
        setReminderParams(-1);
        setReminderDialog();
        setToolbar();
        setFloatingActionButton();
    }

    public void setViews() {

        // reminder dialog view
        reminderDialogView = getLayoutInflater().inflate(R.layout.add_reminder_layout, null, false);
        reminderDateLayout = (LinearLayout) reminderDialogView.findViewById(R.id.reminder_date_layout);
        reminderTimeLayout =  (LinearLayout) reminderDialogView.findViewById(R.id.reminder_time_layout);
        reminderRepetitionLayout =  (LinearLayout) reminderDialogView.findViewById(R.id.reminder_repetition_layout);

        // reminder name
        reminderName = (EditText) reminderDialogView.findViewById(R.id.reminder_name);

        // reminder start date
        reminderStartDate = (TextView) reminderDialogView.findViewById(R.id.reminder_start_date);

        // reminder time
        reminderTime = (TextView) reminderDialogView.findViewById(R.id.reminder_time);

        // reminder count
        reminderCount = (TextView) reminderDialogView.findViewById(R.id.reminder_count);
    }

    public void setReminderListView() {

        // reminder list view
        reminderListView = (ListView) findViewById(R.id.xml_reminders_listview);
        reminderListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // int remID = reminderList.get(position).getReminderID();
                isNewReminder = false;
                selectedReminderID = reminderList.get(position).getReminderID();
                setViews();
                setReminderParams(reminderList.get(position).getReminderID());
                setReminderDialog();
                reminderDialog.show();
                isNewReminder = true;
            }
        });

        // registering context menu for listview
        registerForContextMenu(reminderListView);
    }

    /** This will be invoked when an item in the listview is long pressed */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.actions, menu);
    }

    /** This will be invoked when a menu item is selected */
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        ReminderModel reminderModel = (ReminderModel) reminderListView.getAdapter().getItem(info.position);

        switch(item.getItemId()) {
            case R.id.cnt_mnu_delete:
                mRealm = Realm.getInstance(MainActivity.this);
                ReminderRealmModel reminderRealmModel = mRealm.where(ReminderRealmModel.class).equalTo("reminderID", reminderModel.getReminderID()).findFirst();
                mRealm.beginTransaction();
                deleteScheduledAlarm(getNotification(reminderModel.getReminderName(), reminderModel.getReminderTime()), reminderModel.getReminderID());
                reminderRealmModel.removeFromRealm();
                mRealm.commitTransaction();
                mRealm.close();
                Toast.makeText(this, reminderModel.getReminderName() + " deleted", Toast.LENGTH_SHORT).show();
                updateUI();
                break;
        }
        return true;
    }


    public void setReminderParams(int reminderID) {

        if(isNewReminder) {
            reminderName.setText("");
            reminderStartDate.setText(DateFormat.getDateInstance().format(new Date(Calendar.getInstance().getTimeInMillis())));
            reminderTime.setText((new SimpleDateFormat("hh:mm a")).format(new Date(Calendar.getInstance().getTimeInMillis())));
            reminderCount.setText("No repetition");
            repeatCount = 0;
        } else {
            mRealm = Realm.getInstance(MainActivity.this);
            ReminderRealmModel reminderRealmModel = mRealm.where(ReminderRealmModel.class).equalTo("reminderID", reminderID).findFirst();

            reminderName.setText(reminderRealmModel.getReminderName());
            reminderStartDate.setText(reminderRealmModel.getReminderStartDate()); // DateFormat.getDateInstance().format(new Date(Calendar.getInstance().getTimeInMillis())));
            reminderTime.setText(reminderRealmModel.getReminderTime()); // (new SimpleDateFormat("hh:mm a")).format(new Date(Calendar.getInstance().getTimeInMillis())));
            reminderCount.setText(reminderRealmModel.getReminderRepeat()); // "No repetition");
            repeatCount = getReminderRepeatInt(reminderCount.getText().toString());
            repeatCount = 0;

            mRealm.close();
        }

        Log.i(TAG, "reminder name:  " + reminderName.getText().toString());
        Log.i(TAG, "remidner date:  " + reminderStartDate.getText().toString());
        Log.i(TAG, "reminder time:  " + reminderTime.getText().toString());
        Log.i(TAG, "reminder count: " + reminderCount.getText().toString());
        Log.i(TAG, "repeat count:   "  + String.valueOf(repeatCount));

    }

    public void setReminderDialog() {

        reminderDateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });

        reminderTimeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getSupportFragmentManager(), "timePicker");
            }
        });

        reminderRepetitionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog repeatOptionDialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Repeat");
                builder.setSingleChoiceItems(R.array.repeat, repeatCount, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        repeatCount = which;
                        setReminderRepeatTextView();
                        dialog.dismiss();
                    }
                });
                repeatOptionDialog = builder.create();
                repeatOptionDialog.show();
            }
        });


        // add reminder dialog - dialog builder
        reminderDialogBuilder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
                .setTitle("Add Reminder")
                .setView(reminderDialogView)
                .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // if remindername empty, don't allow user to save
                        Log.i(TAG, "reminder name: " + reminderName.toString());
                        if (isEmpty(reminderName)) {
                            Toast.makeText(MainActivity.this, "Need a name", Toast.LENGTH_SHORT).show();

                        } else {
                            saveReminder();
                            updateUI();
                            dialog.dismiss();
                            Toast.makeText(MainActivity.this, "Reminder Saved", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Toast.makeText(getApplicationContext(), "cancel", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        }
                );
        reminderDialog = reminderDialogBuilder.create();
    }


    public void setFloatingActionButton() {
        floatingActionButton = (FloatingActionButton) findViewById(R.id.xml_add_med_fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setViews();
                setReminderParams(-1);
                setReminderDialog();
                reminderDialog.show();
            }
        });
    }


    public void saveReminder() {

        mRealm = Realm.getInstance(MainActivity.this);
        mRealm.beginTransaction();

        // create reminder
        ReminderRealmModel reminder;

        // delete old reminder alarm
        if(!isNewReminder) {
            reminder = mRealm.where(ReminderRealmModel.class).equalTo("reminderID", selectedReminderID).findFirst();
            deleteScheduledAlarm(getNotification(reminder.getReminderName(), reminder.getReminderTime()),reminder.getReminderID());
            reminder.removeFromRealm();
        }

        // make new reminder alarm
        reminder = new ReminderRealmModel();
        int remID = Settings.sharedInstance(this).getReminderID();
        reminder.setReminderID(remID);
        reminder.setReminderName(reminderName.getText().toString());
        reminder.setReminderStartDate(reminderStartDate.getText().toString());
        reminder.setReminderTime(reminderTime.getText().toString());
        reminder.setReminderRepeat(reminderCount.getText().toString());
        remID++;
        Settings.sharedInstance(this).setReminderID(remID);
        Log.i(TAG, "Added Reminder" + String.valueOf(reminder.getReminderID()) + " " + reminder.getReminderName() + " " + reminder.getReminderStartDate());

        mRealm.copyToRealmOrUpdate(reminder);
        mRealm.commitTransaction();
        mRealm.close();

        scheduleNotification(getNotification(reminder.getReminderName(), reminder.getReminderTime()), reminder.getReminderID(), reminder.getReminderStartDate(), reminder.getReminderTime());
    }


    public void updateUI() {

        reminderList = new ArrayList<>();

        mRealm = Realm.getInstance(this);
        RealmResults<ReminderRealmModel> result = mRealm.where(ReminderRealmModel.class).findAll();
        ReminderRealmModel reminderRealmModel;

        try {
            for(int i = 0; i < result.size(); i++) {
                reminderRealmModel = result.get(i);
                reminderModel = new ReminderModel();
                reminderModel.setReminderID(reminderRealmModel.getReminderID());
                reminderModel.setReminderName(reminderRealmModel.getReminderName());
                reminderModel.setReminderStartDate(reminderRealmModel.getReminderStartDate());
                reminderModel.setReminderTime(reminderRealmModel.getReminderTime());
                reminderModel.setReminderRepeat(reminderRealmModel.getReminderRepeat());
                reminderList.add(reminderModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        reminderAdapter = new ReminderAdapter(this, 0, reminderList);
        reminderListView.setAdapter(reminderAdapter);

        mRealm.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }


    public void scheduleNotification(Notification notification, int id, String startDate, String time) {

        String dateTime = startDate + " " + time;
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getDefault());

        long timeInMillis = 0;
        Date newDate = null;

        try {
            newDate = sdf.parse(dateTime);
            timeInMillis = newDate.getTime();

        } catch (Exception e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeInMillis);
        Log.i(TAG, "Date Time: " + newDate.toString());

        // if alarm time is today, before the current time, shift alarm time into the future by a day
        if(timeInMillis < Calendar.getInstance().getTimeInMillis()) {
            timeInMillis += (1000 * 60 * 60 * 24); // add a day
        }

        Log.i(TAG, "Present Time: " + String.valueOf(Calendar.getInstance().getTimeInMillis()));
        Log.i(TAG, "Future  Time: " + String.valueOf(timeInMillis));

        // create intent
        Intent notificationIntent = new Intent(MainActivity.this, NotificationPublisher.class);
        Log.i(TAG, "id: " + String.valueOf(id));
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, id);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE); // .ALARM_SERVICE);

        switch (repeatCount) {
            case 0:
                alarmManager.set(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent);
                break;
            case 1:
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, timeInMillis, AlarmManager.INTERVAL_HOUR, pendingIntent);
                break;
            case 2:
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, timeInMillis, AlarmManager.INTERVAL_DAY, pendingIntent);
                break;
            case 3:
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, timeInMillis, AlarmManager.INTERVAL_DAY * 7, pendingIntent);
                break;
            case 4:
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, timeInMillis, AlarmManager.INTERVAL_DAY * 30, pendingIntent);
                break;
        }
    }

    public Notification getNotification(String content, String time) {

        String dateTime = reminderStartDate.getText().toString() + " " + time;
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getDefault());

        long timeInMillis = 0;
        Date newDate = null;

        try {
            newDate = sdf.parse(dateTime);
            timeInMillis = newDate.getTime();

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i(TAG, "Notification Time: " + newDate.toString());
        Log.i(TAG, "Present Time: " + String.valueOf(Calendar.getInstance().getTimeInMillis()));
        Log.i(TAG, "Future  Time: " + String.valueOf(timeInMillis));

        Notification.Builder builder = new Notification.Builder(MainActivity.this);
        builder.setContentTitle("Reminder");
        builder.setContentText(content);
        builder.setSmallIcon(R.drawable.ic_alarm);
        builder.setWhen(timeInMillis);
        builder.setLights(0xff00ff00, 300, 100);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        return builder.build();
    }

    public void deleteScheduledAlarm(Notification notification, int id) {
        Intent notificationIntent = new Intent(MainActivity.this, NotificationPublisher.class);
        Log.i(TAG, "id: " + String.valueOf(id));
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, id);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }

    public void setReminderRepeatTextView() {
        switch (repeatCount) {
            case 0:
                repeatText = "No repetition";
                reminderCount.setText(repeatText);
                break;
            case 1:
                repeatText = "Repeat hourly";
                reminderCount.setText(repeatText);
                break;
            case 2:
                repeatText = "Repeat daily";
                reminderCount.setText(repeatText);
                break;
            case 3:
                repeatText = "Repeat weekly";
                reminderCount.setText(repeatText);
                break;
            case 4:
                repeatText = "Repeat monthly";
                reminderCount.setText(repeatText);
                break;
        }
    }

    public int getReminderRepeatInt(String repeatText) {
        int repeatInt = 0;
        switch (repeatText) {
            case "No repetition":
                repeatInt = 0;
                break;
            case "Repeat hourly":
                repeatInt = 1;
                break;
            case "Repeat daily":
                repeatInt = 2;
                break;
            case "Repeat weekly":
                repeatInt = 3;
                break;
            case "Repeat monthly":
                repeatInt = 4;
                break;

        }
        return repeatInt;
    }

    @Override
    public void onFinishedDatePickerDialog(int year, int month, int day) {
        Log.d(TAG, "onFinishedDatePickerDialog(): " + String.valueOf(month) + "/" + String.valueOf(day) + "/" + String.valueOf(year));
        Calendar cal = new GregorianCalendar(year, month, day);
        reminderStartDate.setText(DateFormat.getDateInstance().format(new Date(cal.getTimeInMillis())));
    }

    @Override
    public void onFinishedTimePickerDialog(int hourOfDay, int minute) {
        Log.d(TAG, "onFinishedtimePickerDialog(): " + String.valueOf(hourOfDay) + ":" + String.valueOf(minute));
        long newReminderTime = (new GregorianCalendar(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, hourOfDay, minute, 0)).getTimeInMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        reminderTime.setText(sdf.format(new Date(newReminderTime)));
        // notifyDataSetChanged()
    }

    public void setToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }
}
