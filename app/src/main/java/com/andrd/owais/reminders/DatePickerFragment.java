package com.andrd.owais.reminders;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by owais on 9/25/15.
 */
public class DatePickerFragment extends android.support.v4.app.DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Log.i(getClass().getSimpleName(), "inside onCreateDialog()");

        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {

        // Do something with the time chosen by the user
        DatePickerDialogListener activity = (DatePickerDialogListener) getActivity();
        activity.onFinishedDatePickerDialog(year, month, day);
        Log.i(getClass().getSimpleName(), "SELECTED DATE - year: " + String.valueOf(year) + " month : " + String.valueOf(month) + " day: " + String.valueOf(day));
        this.dismiss();

    }

    public interface DatePickerDialogListener {
        void onFinishedDatePickerDialog(int year, int month, int day);
    }
}
