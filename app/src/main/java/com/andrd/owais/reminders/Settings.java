package com.andrd.owais.reminders;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by owais on 9/25/15.
 */
public class Settings {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public static Settings sharedInstance (Context context) {
        return new Settings(context);
    }

    public Settings(Context context)
    {
        preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    // reminder ID
    public void setReminderID(int ID) { editor.putInt("reminderID", ID).commit(); }
    public int getReminderID() { return preferences.getInt("reminderID", 1); }

    // notification ID
    public void setNotificationID(int ID) {editor.putInt("notificationID", ID).commit(); }
    public int getNotificationID( ) {  return preferences.getInt("notificationID", 1); }

}
