package com.andrd.owais.reminders;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by owais on 9/25/15.
 */

public class ReminderRealmModel extends RealmObject {

    @PrimaryKey
    private int reminderID;

    private String reminderName;
    private String reminderStartDate;
    private String reminderTime;
    private String reminderRepeat;

    public int getReminderID() {
        return reminderID;
    }

    public void setReminderID(int reminderID) {
        this.reminderID = reminderID;
    }

    public String getReminderName() {
        return reminderName;
    }

    public void setReminderName(String reminderName) {
        this.reminderName = reminderName;
    }

    public String getReminderStartDate() {
        return reminderStartDate;
    }

    public void setReminderStartDate(String reminderStartDate) {
        this.reminderStartDate = reminderStartDate;
    }

    public String getReminderTime() {
        return reminderTime;
    }

    public void setReminderTime(String reminderTime) {
        this.reminderTime = reminderTime;
    }

    public String getReminderRepeat() {
        return reminderRepeat;
    }

    public void setReminderRepeat(String reminderRepeat) {
        this.reminderRepeat = reminderRepeat;
    }
}
