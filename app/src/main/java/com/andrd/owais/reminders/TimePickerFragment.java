package com.andrd.owais.reminders;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by owais on 9/25/15.
 */
public class TimePickerFragment extends android.support.v4.app.DialogFragment implements TimePickerDialog.OnTimeSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                android.text.format.DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        // Do something with the time chosen by the user
        TimePickerDialogListener activity = (TimePickerDialogListener) getActivity();
        activity.onFinishedTimePickerDialog(hourOfDay, minute);
        Log.i("selected time", String.valueOf(hourOfDay) + " minute : " + String.valueOf(minute));

        this.dismiss();
    }

    public interface TimePickerDialogListener {
        void onFinishedTimePickerDialog(int hourOfDay, int minute);
    }

}
